 package eu.su.mas.dedaleEtu.mas.agents.yours;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.ExploSoloBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.SayHello;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics.PartageCarateristicsBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics.ReceiveCarateristicsBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.PartageMapBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.PartageNodesBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.ReceiveMapBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.ReceiveNodesBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.silo.PartageSiloPosBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.silo.ReceiveSiloPosBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.tresors.PartageTresorFoundBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.tresors.ReceiveTresorFoundBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.SiloPos;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import eu.su.mas.dedaleEtu.mas.mybehaviours.collectors.CollectRandom;
import eu.su.mas.dedaleEtu.mas.mybehaviours.collectors.CollectSoloBehaviour;
import eu.su.mas.dedaleEtu.mas.mybehaviours.collectors.SearchSiloRandom;
import eu.su.mas.dedaleEtu.mas.mybehaviours.collectors.SearchSiloTactic;
import eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.ExploCoopBehaviour;
import eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.RandomExploAntiBlocage;
import eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.RandomExploCoop;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;

/**
 * ExploreSolo agent. 
 * It explore the map using a DFS algorithm.
 * It stops when all nodes have been visited
 *  
 *  
 * @author hc
 *
 */

public class AgentCollectorEnCoop extends AbstractDedaleAgent {

	private static final long serialVersionUID = -6431752665590433727L;
	private MapRepresentation myMap;
	

	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	private Set<Tresor> tresors;
	private Set<String> closedNodes;
	private List<String> openNodes;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresureFound;
	private Set<Couple<String, Set<Couple<Observation, Integer>>>> allExpertises;
	private SiloPos siloPos;
	private List<SiloPos> siloPos1;

	protected void setup(){

		super.setup();
		final Object[] args = getArguments();
		int poids = 6;
		if(args[0]!=null){
			System.out.println(args[2]);
			poids= (int) args[2];

		}else{
			System.out.println("Erreur lors du tranfert des parametres");
		}		
		List<Behaviour> lb=new ArrayList<Behaviour>();
		this.tresors = new HashSet<Tresor>();
		this.myMap = new MapRepresentation();
		closedNodes=new HashSet<String>();
		openNodes=new ArrayList<String>();
		siloPos = new SiloPos();
		List<SiloPos>siloPos1 = new ArrayList<SiloPos>();
		siloPos1.add(siloPos);

		/************************************************
		 * 
		 * ADD the behaviours 	private Set<Tresor> tresors;
of the Dummy Moving Agent
		 * 
		 ************************************************/
		lb.add(new PartageTresorFoundBehaviour(this,tresors));	
		lb.add(new ReceiveTresorFoundBehaviour(this,tresors));

		
		lb.add(new PartageMapBehaviour( this, this.myMap));
		lb.add(new ReceiveMapBehaviour( this,this.myMap, closedNodes, openNodes));
		
		lb.add(new PartageTresorFoundBehaviour(this,tresors));	
		lb.add(new ReceiveTresorFoundBehaviour(this,tresors));	

		lb.add(new ReceiveSiloPosBehaviour(this,siloPos1));
		
		
//		lb.add(new PartageCarateristicsBehaviour(this,allExpertises));	
//		lb.add(new ReceiveCarateristicsBehaviour(this,allExpertises));	
		
		FSMBehaviour fsm = new FSMBehaviour(this) {
			public int onEnd() {
				System.out.println("FSM behaviour termin�");
				myAgent.doDelete();
				return super.onEnd();
			}

		};
		fsm.registerFirstState(new startMyBehaviours(this,lb), "startup");
		fsm.registerState(new CollectRandom(this), "roaming_random");
		fsm.registerState(new CollectSoloBehaviour(this, myMap, closedNodes, openNodes, poids, tresors),"collect_tactic");

		fsm.registerDefaultTransition("startup","roaming_random");
	
		fsm.registerState(new RandomExploAntiBlocage(this,allExpertises),"randomABS");
		
		fsm.registerState(new SearchSiloRandom(siloPos1),"searchSilo");
		
		fsm.registerState(new SearchSiloTactic(siloPos1, myMap),"goSilo");

		fsm.registerTransition("roaming_random","collect_tactic",2);
		fsm.registerTransition("collect_tactic","roaming_random",2);

		fsm.registerTransition("roaming_random","searchSilo",1);
		fsm.registerTransition("searchSilo","roaming_random",1);
		
		fsm.registerTransition("searchSilo","goSilo",2);
		
		fsm.registerTransition("goSilo","randomABS",2);
		
		fsm.registerTransition("goSilo","searchSilo",3);
		
		fsm.registerTransition("goSilo","roaming_random",1);

		fsm.registerTransition("randomABS","goSilo",1);

		fsm.registerTransition("collect_tactic","searchSilo",1);

		
		
		
		/***
		 * MANDATORY TO ALLOW YOUR AGENT TO BE DEPLOYED CORRECTLY
		 */
		
		
		addBehaviour(fsm);
		
		System.out.println("the  agent "+this.getLocalName()+ " is started");

	}
	
	
	public void beforeMove(){
		super.beforeMove();
		System.out.println("Save everything (and kill GUI) before move");
		this.myMap.prepareMigration();
		}
		/**
		* Method automatically called by Jade after a migration
		*/
	public void afterMove(){
		super.afterMove();
		this.myMap.loadSavedData();
		System.out.println("Restore data (and GUI) after moving");
		}
}
