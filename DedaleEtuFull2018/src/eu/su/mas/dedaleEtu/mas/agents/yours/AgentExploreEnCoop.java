package eu.su.mas.dedaleEtu.mas.agents.yours;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics.ReceiveCaractsBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics.PartageCaractsBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics.PartageCarateristicsBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics.ReceiveCarateristicsBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.PartageMapBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.PartageNodesBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.ReceiveMapBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.ReceiveNodesBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.silo.PartageSiloPosBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.silo.ReceiveSiloPosBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.tresors.PartageTresorFoundBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.tresors.ReceiveTresorFoundBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.Member;
import eu.su.mas.dedaleEtu.mas.knowledge.SiloPos;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.ExploCoopBehaviour;
import eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.RandomExploAntiBlocage;
import eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.RandomExploCoop;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;

/**
 * ExploreSolo agent. 
 * It explore the map using a DFS algorithm.
 * It stops when all nodes have been visited
 *  
 *  
 * @author hc
 *
 */

public class AgentExploreEnCoop extends AbstractDedaleAgent {

	private static final long serialVersionUID = -6431752665590433727L;
	private MapRepresentation myMap ;
	

	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	private Set<Tresor> tresors;
	private Set<String> closedNodes;
	private List<String> openNodes;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorClose;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorOpen;
	private Set<Couple<String,Set<Couple<Observation,Integer>>>> allExpertises;
	private  List<Member> allExpert;
	private SiloPos siloPos;
	// un liste d'etat que l'on utilisera plus tard
	private List<Boolean> state = new ArrayList<Boolean>();
	protected void setup(){
		
		super.setup();
		this.myMap = new MapRepresentation();
		final Object[] args = getArguments();
		int poids = 6;
		if(args[0]!=null){
			System.out.println(args[2]);
			poids= (int) args[2];

		}else{
			System.out.println("Erreur lors du tranfert des parametres");
		}		
		state.add(false); // Parcours fini ou non
		state.add(false); // 
		state.add(false);
		state.add(false);
		
		List<Behaviour> lb=new ArrayList<Behaviour>();
		List<Behaviour> empty=new ArrayList<Behaviour>();
		
	
		
		
//		closedNodes=new HashSet<String>();
//		openNodes=new ArrayList<String>();
		tresors = new HashSet<Tresor>();
		tresorClose = new HashSet< Couple <String,List<Couple<Observation,Integer>>>>();
		tresorOpen = new HashSet< Couple <String,List<Couple<Observation,Integer>>>>();
		allExpertises = new  HashSet< Couple <String,Set<Couple<Observation,Integer>>>>();
		closedNodes=new HashSet<String>();
		openNodes=new ArrayList<String>();
		allExpert = new ArrayList<Member>();
		siloPos = new SiloPos();
		List<SiloPos>siloPos1 = new ArrayList<SiloPos>();
		siloPos1.add(siloPos);

		/************************************************
		 * 
		 * ADD the behaviours of the Dummy Moving Agent
		 * 
		 ************************************************/
		// TODO : Faire un fsm pour le partage ?
		
		lb.add(new PartageMapBehaviour( this, this.myMap));
		lb.add(new ReceiveMapBehaviour( this,this.myMap, closedNodes, openNodes));
		
		lb.add(new PartageTresorFoundBehaviour(this,tresors));	
		lb.add(new ReceiveTresorFoundBehaviour(this,tresors));	
		
		lb.add(new ReceiveCaractsBehaviour(this,allExpert));	
		lb.add(new PartageCaractsBehaviour(this,allExpert));	

		//lb.add(new PartageCarateristicsBehaviour(this,allExpertises));	
		//lb.add(new ReceiveCarateristicsBehaviour(this,allExpertises));	
		

		lb.add(new ReceiveSiloPosBehaviour(this,siloPos1));
		lb.add(new PartageSiloPosBehaviour(this,siloPos1));
	
		FSMBehaviour fsm = new FSMBehaviour(this) {
			public int onEnd() {
				System.out.println("FSM behaviour termin�");
				myAgent.doDelete();
				return super.onEnd();
			}

		};
		fsm.registerFirstState(new startMyBehaviours(this,lb), "startup");			
		fsm.registerState(new ExploCoopBehaviour(this,this.myMap,
				this.closedNodes,this.openNodes,
				tresors,
				poids,tresorClose,tresorOpen), "explore");
		
		fsm.registerDefaultTransition("startup","explore");
		fsm.registerState(new RandomExploCoop(this,allExpertises, tresors),"random");
		fsm.registerState(new RandomExploAntiBlocage(this,allExpertises),"randomABS");

//		fsm.registerTransition("explore","random",1);
		fsm.registerTransition("explore","randomABS",2);
		fsm.registerTransition("randomABS","explore",1);
//		fsm.registerTransition("random","randomABS",2);
		fsm.registerTransition("explore","random",3);

		//fsm.registerTransition("random","caculate",1);
		
		//fsm.registerTransition("tresure_found","a_probleme",0);

		addBehaviour(fsm);
		
		
		
		/***
		 * MANDATORY TO ALLOW YOUR AGENT TO BE DEPLOYED CORRECTLY
		 */
		
		
		//addBehaviour(new startMyBehaviours(this, lb));
		
		System.out.println("the  agent "+this.getLocalName()+ " is started");

	}
	public void beforeMove(){
		super.beforeMove();
		System.out.println("Save everything (and kill GUI) before move");
		this.myMap.prepareMigration();
		}
		/**
		* Method automatically called by Jade after a migration
		*/
	public void afterMove(){
		super.afterMove();
		this.myMap.loadSavedData();
		System.out.println("Restore data (and GUI) after moving");
		}
	
	
}
