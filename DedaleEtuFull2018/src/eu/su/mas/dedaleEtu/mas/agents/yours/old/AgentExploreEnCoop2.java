package eu.su.mas.dedaleEtu.mas.agents.yours.old;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.ExploSoloBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.SayHello;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics.PartageCarateristicsBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics.ReceiveCarateristicsBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.PartageNodesBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes.ReceiveNodesBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.tresors.PartageTresorFoundBehaviour;
import eu.su.mas.dedaleEtu.mas.interactions.protocols.tresors.ReceiveTresorFoundBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.ExploCoopBehaviour;
import jade.core.behaviours.Behaviour;

/**
 * ExploreSolo agent. 
 * It explore the map using a DFS algorithm.
 * It stops when all nodes have been visited
 *  
 *  
 * @author hc
 *
 */

public class AgentExploreEnCoop2 extends AbstractDedaleAgent {

	private static final long serialVersionUID = -6431752665590433727L;
	private MapRepresentation myMap;
	

	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	private Set<String> closedNodes;
	private List<String> openNodes;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorClose;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorOpen;
	private Set<Couple<String,Set<Couple<Observation,Integer>>>> allExpertises;
	// un liste d'etat que l'on utilisera plus tard
	private List<Boolean> state = new ArrayList<Boolean>();
	protected void setup(){

		super.setup();
		myMap = new MapRepresentation();
		final Object[] args = getArguments();
		int poids = 6;
		if(args[0]!=null){
			System.out.println(args[2]);
			poids= (int) args[2];

		}else{
			System.out.println("Erreur lors du tranfert des parametres");
		}		
		state.add(false); // Parcours fini ou non
		state.add(false); // 
		state.add(false);
		state.add(false);
		
		List<Behaviour> lb=new ArrayList<Behaviour>();
		
		closedNodes=new HashSet<String>();
		openNodes=new ArrayList<String>();
		tresorClose = new HashSet< Couple <String,List<Couple<Observation,Integer>>>>();
		tresorOpen = new HashSet< Couple <String,List<Couple<Observation,Integer>>>>();
		allExpertises = new  HashSet< Couple <String,Set<Couple<Observation,Integer>>>>();
		/************************************************
		 * 
		 * ADD the behaviours of the Dummy Moving Agent
		 * 
		 ************************************************/
		lb.add(new ExploSoloBehaviour(this,this.myMap));;	
		
		
		
		
		/***
		 * MANDATORY TO ALLOW YOUR AGENT TO BE DEPLOYED CORRECTLY
		 */
		
		
		addBehaviour(new startMyBehaviours(this, lb));
		
		System.out.println("the  agent "+this.getLocalName()+ " is started");

	}
	
	
	
}
