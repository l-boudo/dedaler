package eu.su.mas.dedaleEtu.mas.agents.yours.old;


import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.knowledge.SiloPos;
import eu.su.mas.dedaleEtu.mas.mybehaviours.collectors.CollectRandom;
import eu.su.mas.dedaleEtu.mas.mybehaviours.collectors.SearchSiloRandom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import dataStructures.tuple.Couple;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;

/**
 * This dummy collector moves randomly, tries all its methods at each time step, store the treasure that match is treasureType 
 * in its backpack and intends to empty its backPack in the Tanker agent. @see {@link RandomWalkExchangeBehaviour}
 * 
 * @author hc
 *
 */
public class DumbCollectorAgentFSM extends AbstractDedaleAgent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1784844593772918359L;
	
	private SiloPos siloPos ;

	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){

		super.setup();
		FSMBehaviour fsm = new FSMBehaviour(this) {
			public int onEnd() {
				System.out.println("FSM behaviour termin�");
//				myAgent.doDelete();
				return super.onEnd();
			}

		};
		
		List<Behaviour> lb=new ArrayList<Behaviour>();
		SiloPos siloPos= new SiloPos();
		//lb.add(new RandomWalkExchangeBehaviour(this));
		
		/*
		 * FSM :
		 * On a : 1 �tat initial, 2 �tat interm�diares, 1 �tat final (inat�gnable normalement)
		 * 
		 * !!! 	On cr�er un �tat Initial qui est startMyBehaviours, on fait ca pour 	!!!
		 * !!! 	que l'agent ne s'execute que quand l'environement a �t� cr��			!!!
		 * 
		 * Voici le graph de transition 
		 * (I)=>(Roam-1)<>(1-TF-0)>(F)
		 * */
		
		fsm.registerFirstState(new startMyBehaviours(this,lb), "startup");			
		fsm.registerDefaultTransition("startup","roaming_random");
		
		fsm.registerState(new CollectRandom(this), "roaming_random");
		fsm.registerState(new SearchSiloRandom(siloPos),"goSilo");
				
		fsm.registerTransition("roaming_random","goSilo",1);
		fsm.registerTransition("goSilo","roaming_random",1);
		fsm.registerTransition("goSilo","roaming_random",2);
		
		fsm.registerTransition("tresure_found","a_probleme",0);

		addBehaviour(fsm);

		System.out.println("the  agent "+this.getLocalName()+ " is started");

	}



	/**
	 * This method is automatically called after doDelete()
	 */
	protected void takeDown(){

	}

}
