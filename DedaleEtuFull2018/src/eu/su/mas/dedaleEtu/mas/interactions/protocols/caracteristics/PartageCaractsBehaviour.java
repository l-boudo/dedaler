package eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.knowledge.Member;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class PartageCaractsBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorClose;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorOpen;
	private List<Member> allExpert;
	private Member me = null;
	private boolean finished = false ;
	
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openedNodes 
	 *  
	 */

	public PartageCaractsBehaviour(final Agent myagent,
			List<Member> allExpert
			) {
		super(myagent);
		this.allExpert = allExpert;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		envoiExpertise();

//		if(treasureFound.isEmpty()) {
//			System.out.println(this.myAgent.getLocalName() +" Exploration successufully done, j'arrete parler.");
//			finished = true;
//		}
		block(3000);
		

	}
	private void envoiExpertise() {
		ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("expertise");
		
			try {
				if(me == null) {
					boolean explo = false;
					if(this.myAgent.getLocalName().charAt(0) == 'E') {
						explo = true;
					}
					
					Set<Couple<Observation,Integer>>t = (Set<Couple<Observation,Integer>>)((AbstractDedaleAgent)this.myAgent).getMyExpertise();
					me = new Member(this.myAgent.getLocalName(),explo,0,((AbstractDedaleAgent)this.myAgent).getMyTreasureType(),t);				
				
					allExpert.add(me);

				}
				msg.setContentObject((Serializable) me);
			} catch (IOException e) {
				e.printStackTrace();
			}
			//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends Close"+tresorClose);
			msg.addReceiver(new AID("Explo1",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Explo3",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Collect1",AID.ISLOCALNAME));

			msg.removeReceiver(this.myAgent.getAID());
			//if(this.myAgent.getLocalName().equals("Explo2")) {msg.addReceiver(new AID("Exp1o1",AID.ISLOCALNAME));}
			
			//if(this.myAgent.getLocalName().equals("Explo1")) {msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));}
						

			//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
			((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		}
	
	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}