package eu.su.mas.dedaleEtu.mas.interactions.protocols.caracteristics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.Coalition;
import eu.su.mas.dedaleEtu.mas.knowledge.Member;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class ReceiveCarateristicsBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */

	private Set<Couple<String,Set<Couple<Observation,Integer>>>> allExpertises;

	private boolean finished =false;

	private List<Member> allExpert;
	
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openNodes 
	 *  
	 */
	public ReceiveCarateristicsBehaviour (
			final Agent myagent,
			Set<Couple<String,Set<Couple<Observation,Integer>>>> allExpertises
			) {
		super(myagent);
		this.allExpertises = allExpertises;
		this.allExpert = allExpert;

		//super(myagent);
	}

	public void action() {
		//1) receive the message
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		if (msg != null && !finished) {		
			try {
				if(msg.getProtocol().equals("expertise")) {
					Member expertise= ((Member) msg.getContentObject());
					System.out.println("Memba "+expertise);
					allExpert.add(expertise);
					String nomAgent = msg.getSender().getLocalName();
					System.out.println("Seize "+allExpert.size());

					if(allExpert.size() == 2) {
						System.out.println(Coalition.generate_coaltions(allExpert,1));
						System.out.println(Coalition.generate_coaltions(allExpert,2));
					}
					//tresorClose.addAll(tresorCloseRecu);
					//System.out.println("For "+this.myAgent.getLocalName()+""+recievedExpertise);
					//System.out.println("For "+this.myAgent.getLocalName()+" Current List is open"+openNodes.toString());
					
					//System.out.println(this.myAgent.getLocalName()+"<----Result received from "+msg.getSender().getLocalName()+" ,content closed= "+tresorCloseRecu.toString());
				}
			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			
			block();// the behaviour goes to sleep until the arrival of a new message in the agent's Inbox.
		}
	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		/*
		 * Quand c'est fini, on calcul les stratégies
		 * */
		return finished;
	}
}