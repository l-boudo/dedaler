package eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class PartageMapBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */

	private boolean finished = false ;

	private MapRepresentation myMap;
	
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openedNodes 
	 *  
	 */
	public PartageMapBehaviour (final Agent myagent,MapRepresentation myMap) {
		super(myagent);
		this.myMap= myMap;


		//super(myagent);
	}

	@Override
	public void action() {
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		envoi();
		//System.out.println("ACTION "+this.myAgent.getLocalName());

//		if( !(closedNodes.isEmpty()) && openNodes.isEmpty()) {
//			System.out.println(this.myAgent.getLocalName() +" Exploration successufully done, j'arrete parler.");
//			finished = true;
//		}
		block(500);
		

	}
	private void envoi() {
		ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("map");
		if (myMap!=null){
			try {
				msg.setContentObject((Serializable) myMap.getSerialiazableData());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
			msg.addReceiver(new AID("Explo1",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Collect1",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Collect2",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Collect3",AID.ISLOCALNAME));

			msg.removeReceiver(this.myAgent.getAID());
			((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		}	
		else {
			msg.setProtocol("nomap");
		}
	}
	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}