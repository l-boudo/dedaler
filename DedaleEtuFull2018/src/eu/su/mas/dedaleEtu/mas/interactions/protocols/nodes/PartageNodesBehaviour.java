package eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class PartageNodesBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */
	private Set<String> closedNodes;

	private List<String> openNodes;
	
	private boolean finished = false ;
	
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openedNodes 
	 *  
	 */
	public PartageNodesBehaviour (final Agent myagent,Set<String> closedNodes, List<String> openNodes) {
		super(myagent);
		this.closedNodes = closedNodes;
		this.openNodes = openNodes;

		//super(myagent);
	}

	@Override
	public void action() {
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		envoiListClosed(closedNodes);
		envoiListOpen(openNodes);
		if( !(closedNodes.isEmpty()) && openNodes.isEmpty()) {
			System.out.println(this.myAgent.getLocalName() +" Exploration successufully done, j'arrete parler.");
			finished = true;
		}
		block(3000);
		

	}
	private void envoiListClosed(Set l) {
		ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("closednodeslist");
		if (!l.isEmpty()){
//			System.out.println("List Open Empty "+op.isEmpty());
			try {
				msg.setContentObject((Serializable) l);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
			msg.addReceiver(new AID("Explo1",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Explo3",AID.ISLOCALNAME));
			msg.removeReceiver(this.myAgent.getAID());
			//if(this.myAgent.getLocalName().equals("Explo2")) {msg.addReceiver(new AID("Exp1o1",AID.ISLOCALNAME));}
			
			//if(this.myAgent.getLocalName().equals("Explo1")) {msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));}
						

			//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
			((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		}	
	}
	private void envoiListOpen(List l) {
		ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("opennodeslist");
		
		if (!l.isEmpty()){
			try {
				msg.setContentObject((Serializable) l);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
			msg.addReceiver(new AID("Explo1",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));
			msg.addReceiver(new AID("Explo3",AID.ISLOCALNAME));

			msg.removeReceiver(this.myAgent.getAID());
			//if(this.myAgent.getLocalName().equals("Explo2")) {msg.addReceiver(new AID("Exp1o1",AID.ISLOCALNAME));}
			
			//if(this.myAgent.getLocalName().equals("Explo1")) {msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));}
						

			//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
			((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		}	
		else {
			msg.setProtocol("opennodeslistEmpty");
		}
	}
	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}