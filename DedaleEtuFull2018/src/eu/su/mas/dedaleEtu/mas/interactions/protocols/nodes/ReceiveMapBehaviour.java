package eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes;

import java.util.List;
import java.util.Set;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.Local;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class ReceiveMapBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */
	
	private boolean finished =false;
	
	private Set<String> closedNodes;

	private List<String> openNodes;
	
	private MapRepresentation myMap;

	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openNodes 
	 *  
	 */
	public ReceiveMapBehaviour (final Agent myagent,MapRepresentation myMap,Set<String> closedNodes, List<String> openNodes) {
		super(myagent);
		this.closedNodes = closedNodes;
		this.openNodes = openNodes;
		this.myMap= myMap;

	}

	public void action() {
		
		//1) receive the message
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		
//		if( !(closedNodes.isEmpty()) && openNodes.isEmpty()) {
//			System.out.println(this.myAgent.getLocalName() +" Exploration successufully done.");
//			finished = true;
//		}
		if (msg != null && !finished) {	
			
			try {

				if(msg.getProtocol().equals("map")) {

					List<List<String>>  map = (List<List<String>>) msg.getContentObject();
					
					myMap.merge(map);
					
					closedNodes.addAll(myMap.getCloseNodes());
					openNodes.removeAll(myMap.getCloseNodes());
					

				}


			} catch (UnreadableException e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
			}
		}else{
			
			block();// the behaviour goes to sleep until the arrival of a new message in the agent's Inbox.
		}
	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}