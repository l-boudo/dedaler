package eu.su.mas.dedaleEtu.mas.interactions.protocols.nodes;

import java.util.List;
import java.util.Set;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class ReceiveNodesBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */
	private Set<String> closedNodes;

	private List<String> openNodes;
	
	private boolean finished =false;
	
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openNodes 
	 *  
	 */
	public ReceiveNodesBehaviour (final Agent myagent,Set<String> closedNodes, List<String> openNodes) {
		super(myagent);
		this.closedNodes = closedNodes;
		this.openNodes = openNodes;
		//super(myagent);
	}

	public void action() {
		
		//1) receive the message
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		if( !(closedNodes.isEmpty()) && openNodes.isEmpty()) {
			System.out.println(this.myAgent.getLocalName() +" Exploration successufully done.");
			finished = true;
		}
		if (msg != null && !finished) {		
			try {
				if(msg.getProtocol().equals("closednodeslist")) {
					Set close = ((Set<String>)msg.getContentObject());
					//System.out.println("For "+this.myAgent.getLocalName()+" Current List is closed"+closedNodes.toString());
					
					closedNodes.addAll(close);
					openNodes.removeAll(close);
				}
				else if(msg.getProtocol().equals("opennodeslist")) {
					List open = ((List<String>)msg.getContentObject());
				}

			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			
			block();// the behaviour goes to sleep until the arrival of a new message in the agent's Inbox.
		}
	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}