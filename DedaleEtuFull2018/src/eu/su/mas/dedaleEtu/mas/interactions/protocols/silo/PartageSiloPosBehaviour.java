package eu.su.mas.dedaleEtu.mas.interactions.protocols.silo;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.knowledge.SiloPos;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class PartageSiloPosBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */

	private boolean finished = false ;

	private SiloPos siloPos;
	private List<SiloPos> siloPos1;

	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openedNodes 
	 *  
	 */
	
	
	
	public PartageSiloPosBehaviour(final Agent myagent,List<SiloPos> siloPos1
			) {
		super(myagent);
		this.siloPos1=siloPos1;

		//this.allExpertises = allExpertises;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {
		//String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

		if(siloPos1.get(0).met()) {

			envoiPosition();			
		}
		block(300);
		

	}
	private void envoiPosition() {
		ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("siloPos");
		//siloPos = siloPos1.get(0);
//		System.out.println(this.myAgent.getLocalName()+"partage la position du silo "+siloPos1.get(0));
	
		
		try {
			msg.setContentObject(siloPos1.get(0));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
		msg.addReceiver(new AID("Explo1",AID.ISLOCALNAME));
		msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));
		msg.addReceiver(new AID("Collect1",AID.ISLOCALNAME));
		msg.addReceiver(new AID("Collect2",AID.ISLOCALNAME));
		msg.addReceiver(new AID("Collect3",AID.ISLOCALNAME));
		
		msg.removeReceiver(this.myAgent.getAID());
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		
	}
	
	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}