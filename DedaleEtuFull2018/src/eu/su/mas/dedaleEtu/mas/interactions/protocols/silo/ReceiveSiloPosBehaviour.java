package eu.su.mas.dedaleEtu.mas.interactions.protocols.silo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.knowledge.SiloPos;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class ReceiveSiloPosBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */

	private boolean finished =false;

	private SiloPos siloPos;
	private List<SiloPos> siloPos1;

	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openNodes 
	 *  
	 */
	public ReceiveSiloPosBehaviour (
			final Agent myagent,
			SiloPos siloPos
			) {
		super(myagent);
		this.siloPos = siloPos;
		//super(myagent);
	}

	public ReceiveSiloPosBehaviour(final Agent myagent, List<SiloPos> siloPos1) {
		// TODO Auto-generated constructor stub
		super(myagent);

		this.siloPos1=siloPos1;

	}

	public void action() {
		//1) receive the message
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		if (msg != null && !finished) {		
			try {
				if(msg.getProtocol().equals("myPos")||msg.getProtocol().equals("siloPos")) {
					
					SiloPos s = (SiloPos) msg.getContentObject();
					if(s!=null) siloPos1.set(0, siloPos1.get(0).newer(s));
//					System.out.println(this.myAgent.getLocalName()+" :- J'ai recu cette position du silo "+siloPos1.get(0));
				
				}
			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			
			block();// the behaviour goes to sleep until the arrival of a new message in the agent's Inbox.
		}
	//	System.out.println(this.myAgent.getLocalName()+"<----Result received from "+ msg.getSender().getLocalName() +" , siloPosition= "+position);
	}
	

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		/*
		 * Quand c'est fini, on calcul les strat�gies
		 * */
		return finished;
	}
}