package eu.su.mas.dedaleEtu.mas.interactions.protocols.tresors;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor_test;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class PartageTresorFoundBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */
	private Set tresors;
	private boolean finished = false ;
	
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openedNodes 
	 *  
	 */


	public PartageTresorFoundBehaviour(final Agent myagent, Set tresors) {
		// TODO Auto-generated constructor stub
		super(myagent);
		this.tresors = tresors;
	}

	@Override
	public void action() {
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		envoi();
		block(500);
		

	}
	private void envoi() {
		ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("tresor");
		
			try {
				msg.setContentObject((Serializable) tresors);
//				System.out.println(this.myAgent.getLocalName()+":- Envoi "+ tresors);
//				msg.setContentObject((Serializable) new HashSet<Tresor>());

			} catch (IOException e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
		}
//		System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");
		msg.addReceiver(new AID("Explo1",AID.ISLOCALNAME));
		msg.addReceiver(new AID("Explo2",AID.ISLOCALNAME));
		msg.addReceiver(new AID("Collect1",AID.ISLOCALNAME));
		msg.addReceiver(new AID("Collect2",AID.ISLOCALNAME));
		msg.addReceiver(new AID("Collect3",AID.ISLOCALNAME));

		msg.removeReceiver(this.myAgent.getAID());
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);

	}
	
	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}