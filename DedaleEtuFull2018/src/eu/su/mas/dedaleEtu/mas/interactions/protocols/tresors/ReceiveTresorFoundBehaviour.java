package eu.su.mas.dedaleEtu.mas.interactions.protocols.tresors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class ReceiveTresorFoundBehaviour extends SimpleBehaviour{

	/**
	 * Visited nodes
	 */

	private Set<Couple<String,List<Couple<Observation,Integer>>>>tresorClose;
	private Set<Couple<String,List<Couple<Observation,Integer>>>>tresorOpen;
	private Set<Tresor> tresors;
	private boolean finished =false;
	
	private static final long serialVersionUID = -2058134622078521998L;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 * @param openNodes 
	 *  
	 */

	public ReceiveTresorFoundBehaviour(final Agent myagent, Set<Tresor> tresors) {
		// TODO Auto-generated constructor stub
		super(myagent);
		this.tresors = tresors;
		}

	public void action() {
		//1) receive the message
		final MessageTemplate msgTemplate = MessageTemplate.MatchPerformative(ACLMessage.INFORM);			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		if (msg != null && !finished) {		
			try {
				
				if(msg.getProtocol().equals("tresor")) {
					boolean eq = false;
//					System.out.println("the  agent "+((AbstractDedaleAgent)this.myAgent).getLocalName()+ " a recu le treshor "+msg.getContentObject() + 
//							" from "+msg.getSender().getLocalName());
//					System.out.println("Agent "+this.myAgent.getLocalName()+ tresors);
//					System.out.println("Agent "+this.myAgent.getLocalName()+" recoit "+(Set<Tresor>) msg.getContentObject()+" de "+msg.getSender().getLocalName());
					List<String> pos_dejapris = new ArrayList<String>();
					List<String> pos_dejaouvert = new ArrayList<String>();
					Set<Tresor> tresors_recu = (Set<Tresor>) msg.getContentObject();
					for(Tresor t : tresors_recu) {
						if(!t.still_exist()) {
							pos_dejapris.add(t.getPos());
						}
						if(t.isOpen()) {
							pos_dejaouvert.add(t.getPos());
						}
						
					}
					for(Tresor t : (Set<Tresor>) msg.getContentObject()) {
						if((pos_dejapris.contains(t.getPos()) && t.still_exist())|| (!t.isOpen() && pos_dejaouvert.contains(t.getPos())) ) {
							tresors_recu.remove(t);
						}
						
					}
					for(Tresor t : tresors_recu) {
						if(!tresors.contains(t)) {
							tresors.add(t);
						}
					}
//					System.out.println("LAALFIN "+this.myAgent.getLocalName()+" recoit "+(Set<Tresor>) msg.getContentObject()+" de "+msg.getSender().getLocalName());

					
				}

			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			
			block();// the behaviour goes to sleep until the arrival of a new message in the agent's Inbox.
		}
	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}