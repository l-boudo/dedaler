package eu.su.mas.dedaleEtu.mas.knowledge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;

public class Coalition implements Comparator<Coalition>{
	private List<Member> members = new ArrayList<Member>();
	private List<Integer> skills = new ArrayList<Integer>();
	private int lockpick=0 ;
	private int strength=0 ;
	private int backpack=0 ;
	private Set<Tresor> tresors ;
	private int utility = 0;
	public Coalition() {
		
	}
	public Coalition(List<Member> members, Set<Tresor> tresors) {
		super();
		this.members.addAll(members);
		for(Member m : members) {
			lockpick += m.getLockpick();
			strength+= m.getStrengh();
			backpack += m.getBackpack();
			utility += lockpick + strength + 0.3*backpack;

		}
		this.tresors=tresors;
//		skills.set(0, lockpick);
//		skills.set(1, strength);
//		skills.set(2, backpack);
	}
	public Coalition(List<Member> members) {
		super();
		this.members.addAll(members);
		for(Member m : members) {
			lockpick += m.getLockpick();
			strength+= m.getStrengh();
			backpack += m.getBackpack();
			utility += lockpick + strength + 0.3*backpack;

		}
//		skills.set(0, lockpick);
//		skills.set(1, strength);
//		skills.set(2, backpack);
	}
	public void addMember(Member member) {
		if(!members.contains(member)) {
			members.add(member);
			lockpick += member.getLockpick();
			strength+= member.getStrengh();
			backpack += member.getBackpack();
			utility += lockpick + strength + 0.3*backpack; 
		}
	}

	public List<Member> getMembers() {
		return members;
	}
	public void setMembers(List<Member> members) {
		this.members.clear();
		this.members.addAll(members);
		lockpick 	=0;
		strength	=0;
		backpack	=0;
		utility 	=0;
		for(Member m : members) {
			lockpick += m.getLockpick();
			strength+= m.getStrengh();
			backpack += m.getBackpack();
			utility += lockpick + strength + 0.3*backpack; 
		}
	}

	
	
	public int getUtility() {
		return utility;
	}
	public void setUtility(int utility) {
		this.utility = utility;
	}
	public void addUtility(int utility) {
		this.utility += utility;
	}
	public int getLockpick() {
		return lockpick;
	}
	public void setLockpick(int lockpick) {
		this.lockpick = lockpick;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public int getBackpack() {
		return backpack;
	}
	public void setBackpack(int backpack) {
		this.backpack = backpack;
	}
	public Set<Tresor> getTresors(){
		return tresors;
	}
	public void setTresors(Set<Tresor> tresors) {
		this.tresors = tresors;
	}
	public static List<Coalition> generate_coaltions(List<Member> members, int size){
		// generate size 1 coalition
		Set<Member> setMembers = new HashSet<Member>(members);
		Set<Set<Member>> powerMember = powerSet(setMembers);
		
		List<Member> nmembers = new ArrayList<Member>(members);
		List<Coalition> all_coalitions = new ArrayList<Coalition>();
		for(Set s : powerMember) {
			if(s.size()==size) {
				all_coalitions.add(new Coalition(new ArrayList<Member>(s)));				
			}
		}
		// generate other size ( max = size of allmembers)
	
		return all_coalitions;
	}
	public static List<Coalition> generate_coaltions_wu(List<Member> members, int size,Set<Tresor> tresors,boolean explo){
		// generate size 1 coalition
		Set<Member> setMembers = new HashSet<Member>(members);
		Set<Set<Member>> powerMember = powerSet(setMembers);
		
		List<Member> nmembers = new ArrayList<Member>(members);
		List<Coalition> all_coalitions = new ArrayList<Coalition>();
		for(Set s : powerMember) {
			if(s.size()==size) {
				all_coalitions.add(new Coalition(new ArrayList<Member>(s),tresors));				
			}
		}
		if(explo){
			for (Tresor t : tresors) {
				if(t.still_exist()&&!t.isOpen()) 
				{
					for(Coalition c : all_coalitions) {
						if(t.getLockpick()<=c.getLockpick())
						{
							c.addUtility(10);
						}
					}
				}
					
			}
		}
		
		// generate other size ( max = size of allmembers)
	
		return all_coalitions;
	}
	public static List<Coalition> generate_struct(List<Member> members){
		// generate size 1 coalition
		Set<Member> setMembers = new HashSet<Member>(members);
		Set<Set<Member>> powerMember = powerSet(setMembers);
		Set<Coalition> all_coalitions = new HashSet<Coalition>();
		
		
		Set<Set<Coalition>> allStruct = new HashSet<Set<Coalition>>() ;
	
//		System.out.println("POWERSET " + powerMember);
		List<Set<Member>> powerMember2 = new ArrayList<Set<Member>>((Collection<? extends Set<Member>>) setMembers);
		
		
		int size;
		for(Set<Member> me : powerMember) {
			Set<Set<Member>> struct =  new HashSet<Set<Member>>() ;
			struct.add(me);
			size=me.size();
			for(Set<Member> me2 : powerMember) {
				if(!Collections.disjoint(me, me2)) {
					struct.add(me2);
					size+=me2.size();
				}				
				if(size==members.size()) {
//					allStruct.add(struct);
					struct =  new HashSet<Set<Member>>();
					struct.add(me);
					size=me.size();
					continue;
				}
			}
		}
		
		System.out.println(allStruct);

		// generate other size ( max = size of allmembers)
	
		return null;
	}
	public static void generate_struct20(Set<Set<Member>> members,Set<Set<Member>> powermembers){
		Set<Set<Member>> new_members = new HashSet<Set<Member>>(members);
		Set<Set<Member>> new_members_copy = new HashSet<Set<Member>>(members);
		Set<Set<Member>> new_powermembers = new HashSet<Set<Member>>(powermembers);
			if (new_powermembers.isEmpty()) {
			}
			new_powermembers = new HashSet<Set<Member>>(powermembers);

			new_members = new HashSet<Set<Member>>(members);
			
		}
	
	
	public static Set<Set<Member>> generate_coaltions20(List<Member> members){
		// generate size 1 coalition
		Set<Member> setMembers = new HashSet<Member>(members);
		Set<Set<Member>> powerMember = powerSet(setMembers);
		Iterator<Set<Member>> it= powerMember.iterator();
		while(it.hasNext()) {
			if(it.next().size()==0) {
				it.remove();
			}
		}
		// generate other size ( max = size of allmembers)
	
		return powerMember;
	}

	
	@Override
	public String toString() {
		return "Coalition [members=" + members + ", lockpick=" + lockpick + ", strength=" + strength + ", backpack="
				+ backpack + ", utility=" + utility + "]";
	}
/**TODO: POWERSET
	 * On genere les coalition de maniere recursive
	 * Le principe est simple :
	 *  - On creer de 
	 *  - Puis on itere 
	 * */
/**
 * https://stackoverflow.com/questions/1670862/obtaining-a-powerset-of-a-set-in-java
 * */
	public static <T> Set<Set<T>> powerSet(Set<T> originalSet) {
	    Set<Set<T>> sets = new HashSet<Set<T>>();
	    if (originalSet.isEmpty()) {
	        sets.add(new HashSet<T>());
	        return sets;
	    }
	    List<T> list = new ArrayList<T>(originalSet);
	    T head = list.get(0);
	    Set<T> rest = new HashSet<T>(list.subList(1, list.size())); 
	    for (Set<T> set : powerSet(rest)) {
	        Set<T> newSet = new HashSet<T>();
	        newSet.add(head);
	        newSet.addAll(set);
	        sets.add(newSet);
	        sets.add(set);
	    }       
	    return sets;
	}

	public static Set<Set<Member>> generate_powerSet(Set<Member> setMembers) {
		return powerSet(setMembers);
	}
	@Override
	public int compare(Coalition o1, Coalition o2) {
		// TODO Auto-generated method stub
		if(o1.utility<o2.utility) {
			return -1;
		}
		else if(o1.utility>o2.utility) {
			return 1;
		}
		return 0;
	}
	public static Coalition getMax(Collection coals) {
		return Collections.max(coals,
			new Comparator<Coalition>() {
				public int compare(Coalition o1, Coalition o2) {
					// TODO Auto-generated method stub
					if(o1.getUtility()<o2.getUtility()) {
						return -1;
					}
					else if(o1.getUtility()>o2.getUtility()) {
						return 1;
					}
					return 0;
				}
		});
	}
}

