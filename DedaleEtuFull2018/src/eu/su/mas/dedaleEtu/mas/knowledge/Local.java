package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.Collection;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

public class Local implements Serializable {
	public Collection<Node> nodes;
	public Collection<Edge> edges;
	public Local(Collection<Node> nodeSet, Collection<Edge> edgeSet) {
		// TODO Auto-generated constructor stub
		this.nodes = nodeSet;
		this.edges = edgeSet;
	}
}
