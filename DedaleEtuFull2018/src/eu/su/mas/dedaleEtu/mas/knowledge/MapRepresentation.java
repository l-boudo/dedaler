package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.Edge;
import org.graphstream.graph.EdgeRejectedException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

/**
 * This simple topology representation only deals with the graph, not its content.</br>
 * The knowledge representation is not well written (at all), it is just given as a minimal example.</br>
 * The viewer methods are not independent of the data structure, and the dijkstra is recomputed every-time.
 * 
 * @author hc
 */
public class MapRepresentation implements Serializable {

	public enum MapAttribute {
		agent,open
	}

	private static final long serialVersionUID = -1333959882640838272L;

	private Graph g; //data structure
	private Viewer viewer; //ref to the display
	
	private Integer nbEdges;//used to generate the edges ids
	
	/*********************************
	 * Parameters for graph rendering
	 ********************************/
	
	private String defaultNodeStyle= "node {"+"fill-color: black;"+" size-mode:fit;text-alignment:under; text-size:14;text-color:white;text-background-mode:rounded-box;text-background-color:black;}";
	private String nodeStyle_open = "node.agent {"+"fill-color: forestgreen;"+"}";
	private String nodeStyle_agent = "node.open {"+"fill-color: blue;"+"}";
	private String nodeStyle=defaultNodeStyle+nodeStyle_agent+nodeStyle_open;
	private SerializableSimpleGraph<String, MapAttribute> sg;
	// index 0 => nodes
	// index 1 => edges 
	// index 2 =>  ?

	private List<List<String>> serializedGraph;

	private Set<String> nodes;
	public MapRepresentation() {
		System.setProperty("org.graphstream.ui.renderer","org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		
		this.g= new SingleGraph("My world vision");
		this.g.setAttribute("ui.stylesheet",nodeStyle);
		//this.viewer = this.g.display();
		this.nbEdges=0;
	}

	/**
	 * Associate to a node an attribute in order to identify them by type. 
	 * @param id
	 * @param mapAttribute
	 */
	public void addNode(String id,MapAttribute mapAttribute){
		Node n;
		if (this.g.getNode(id)==null){
			n=this.g.addNode(id);
		}else{
			n=this.g.getNode(id);
		}
		n.clearAttributes();
		n.addAttribute("ui.class", mapAttribute.toString());
		n.addAttribute("ui.label",id);
	}
	/**
	 * Associate to a node an attribute in order to identify them by type. 
	 * @param id
	 * @param mapAttribute
	 */
	public void addNode(String id,String mapAttribute){
		Node n;
		String old_map;
		if (this.g.getNode(id)==null){
			n=this.g.addNode(id);
		}else{
			n=this.g.getNode(id);
			old_map = this.g.getNode(id).getAttribute("ui.class");
			if(old_map==null) {
				if(mapAttribute!=null) {					
					mapAttribute=null;
				}
			}
		}

		n.clearAttributes();
		n.addAttribute("ui.class", mapAttribute);
		n.addAttribute("ui.label",id);
	}

	/**
	 * Add the node id if not already existing
	 * @param id
	 */
	public void addNode(String id){
		Node n=this.g.getNode(id);
		if(n==null){
			n=this.g.addNode(id);
		}else{
			n.clearAttributes();
		}
		n.addAttribute("ui.label",id);
	}

   /**
    * Add the edge if not already existing.
    * @param idNode1
    * @param idNode2
    */
	public void addEdge(String idNode1,String idNode2){
		try {
			this.nbEdges++;
			this.g.addEdge(this.nbEdges.toString(), idNode1, idNode2);
		}catch (EdgeRejectedException e){
			//Do not add an already existing one
			this.nbEdges--;
		}
		
	}

	/**
	 * Compute the shortest Path from idFrom to IdTo. The computation is currently not very efficient
	 * 
	 * @param idFrom id of the origin node
	 * @param idTo id of the destination node
	 * @return the list of nodes to follow
	 */
	public List<String> getShortestPath(String idFrom,String idTo){
		List<String> shortestPath=new ArrayList<String>();

		Dijkstra dijkstra = new Dijkstra();//number of edge
		dijkstra.init(g);
		dijkstra.setSource(g.getNode(idFrom));
		dijkstra.compute();//compute the distance to all nodes from idFrom
		List<Node> path=dijkstra.getPath(g.getNode(idTo)).getNodePath(); //the shortest path from idFrom to idTo
		Iterator<Node> iter=path.iterator();
		while (iter.hasNext()){
			shortestPath.add(iter.next().getId());
		}
		dijkstra.clear();
		shortestPath.remove(0);//remove the current position
		return shortestPath;
	}
	   /**
	    * Returns the serialized data.
	    * 
	    */
	public List<List<String>> getSerialiazableData() {
		this.serializedGraph = new ArrayList<List<String>>();
		List nodesList = new ArrayList<String>();
		List edgesList = new ArrayList<String>();
		for( Node n :g.getNodeSet()) {
			nodesList.add( n.getAttribute("ui.label")+" "+n.getAttribute("ui.class") );
			//System.out.println("SERIAL "+n.getAttribute("ui.label")+" "+n.getAttribute("ui.class"));

		}
		this.serializedGraph.add(nodesList);
		for( Edge e :g.getEdgeSet()) {
			edgesList.add( e.getIndex()+" "+e.getNode0()+" "+e.getNode1() );
		}
		this.serializedGraph.add(edgesList);
		return serializedGraph;
		
	}
   /**
    * Prepares the container migration.
    */
	public void prepareMigration(){
		//this.sg= new SerializableSimpleGraph<String,MapAttribute>();
		this.serializedGraph = new ArrayList<List<String>>();
		//1) Réalise la copie de g (non sérializable) dans sg.
		getSerialiazableData();
		//2) On détruit les refs vers les objets non sérializables
		if (this.viewer!=null){
			System.out.println(this.viewer);
//			try {				
//				this.viewer.close();
//			} catch (Exception e) {
//				// TODO: handle exception
//				//e.printStackTrace();
//			}
			this.viewer=null;
			this.g=null;

		}
		System.out.println("Je prepare ma migration !!!");
		this.g=null;
	}
   /**anObject
    * Reloads the data after the container migration.
    */
	public void loadSavedData(){
		this.g= new SingleGraph("My world vision");
		this.g.setAttribute("ui.stylesheet",nodeStyle);
		this.viewer = this.g.display();
		Integer nbEd=0;
		String[] serialized_node;
		Node node;
		
		//Parcours de sg et remplissage de g.
		System.out.println("BOOM");
		for(String n : serializedGraph.get(0)) {
			//remplir les nodes
			serialized_node = n.split(" ");
			addNode(serialized_node[0],serialized_node[1]);
		}
		String[] serialized_edge;
		for(String e : serializedGraph.get(1)) {
			//remplir les edges
			serialized_edge = e.split(" ");
			addEdge(serialized_edge[1],serialized_edge[2]);
			//g.addEdge( nexG[0] , nexG[1] , nexG[2] );
		}
		System.out.println("Loading done");
	}
	
	/**
	 * Merges the serialized List with the current Graph.
	 * We get the list then we check for each edges if it's already in the graph
	 * if not we add it to the current graph and for nodes we just check if the 
	 * MapAttribute is different from "agent"
	 * @param serialized_graph the serialized data
	 * */
	public void merge(List<List<String>> serialized_graph) {

		Node added_node;
		String[] serialized_node;
		String[] serialized_edge;

		boolean already_in = false;
		
		for(String n : serialized_graph.get(0)) {

			serialized_node = n.split(" ");
//			if(serialized_node[1].equals(MapAttribute.agent.toString())) serialized_node[1] = null;
			addNode(serialized_node[0],serialized_node[1]);
			already_in = false;
		}
		
		for(String e : serialized_graph.get(1)) {
			serialized_edge = e.split(" ");
			for (Edge ed : g.getEdgeSet()) {
				if( ed.getNode0().equals(serialized_edge[1]) && ed.getNode1().equals(serialized_edge[2])||
					ed.getNode0().equals(serialized_edge[2]) && ed.getNode1().equals(serialized_edge[1]) ) {
					already_in = true;
				}
			}
			if(!already_in) {
				//System.out.println(serialized_graph.get(0));
				addEdge(serialized_edge[1],serialized_edge[2]);
			}
			already_in = false;
		}	
	}
	public Set<String> getCloseNodes(){
		Set<String>closeNodes = new HashSet<String>();
		for(Node node : g.getNodeSet()) {
			if(node.getAttribute("ui.class") == null) {
				closeNodes.add(node.getId());
			}
		}
		return closeNodes;
	}
//	public List<String> getOpenNodes(){
//		return null;
//	}
}
