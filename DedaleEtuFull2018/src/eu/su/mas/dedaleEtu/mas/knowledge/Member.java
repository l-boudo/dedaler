package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;

public class Member implements Serializable{
	private String localName;
	private boolean explo = true;
	private int backpack = -1;
	private int lockpick = -1;
	private int strength = -1;
	private Observation type = null;
	public Member(String localName, boolean explo, int backpack, int lockpick, int strengh,Observation type) {
		super();
		
		this.localName = localName;
		this.explo = explo;
		
		this.backpack = backpack;
		this.lockpick = lockpick;
		
		this.strength = strengh;
		this.type = type;
	}
	public Member(String localName, boolean explo) {
		super();
		
		this.localName = localName;
		this.explo = explo;
		Random r= new Random();
		this.lockpick = r.nextInt(10);

	}
	public Member(String localName, int lock) {
		super();
		
		this.localName = localName;
		this.explo = explo;
		this.lockpick = lock;

	}
	public Member(String localName,boolean explo, int backpack,Observation type,Set<Couple<Observation, Integer>> member) {
		super();
		
		for(Couple<Observation,Integer> couple : member) {
			if(couple.getLeft().equals(Observation.STRENGH)) {
				this.strength = couple.getRight();
			}
			else if(couple.getLeft().equals(Observation.LOCKPICKING)) {
				this.lockpick = couple.getRight();

			}
		}
		
		this.localName = localName;
		this.explo = explo;
		
		this.backpack = backpack;
		this.type = type;
	}
	public String getLocalName() {
		return localName;
	}
	public void setLocalName(String localName) {
		this.localName = localName;
	}
	public boolean isExplo() {
		return explo;
	}
	public void setExplo(boolean explo) {
		this.explo = explo;
	}
	public int getBackpack() {
		return backpack;
	}
	public void setBackpack(int backpack) {
		this.backpack = backpack;
	}
	public int getLockpick() {
		return lockpick;
	}
	public void setLockpick(int lockpick) {
		this.lockpick = lockpick;
	}
	public int getStrengh() {
		return strength;
	}
	public void setStrengh(int strengh) {
		this.strength = strengh;
	}
	
	public Observation getType() {
		return type;
	}
	public void setType(Observation type) {
		this.type = type;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localName == null) ? 0 : localName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Member other = (Member) obj;
		if (localName == null) {
			if (other.localName != null)
				return false;
		} else if (!localName.equals(other.localName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Member [localName=" + localName + ", explo=" + explo + ", backpack=" + backpack + ", lockpick="
				+ lockpick + ", strength=" + strength + ", type=" + type + "]";
	}

	
}
