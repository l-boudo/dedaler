package eu.su.mas.dedaleEtu.mas.knowledge;

import jade.util.leap.Serializable;

public class SiloPos implements Serializable {
	
	private String position;
	private long time ;
	public SiloPos(String position, long time) {
		super();
		this.position = position;
		this.time = time;
	}
	public SiloPos() {
		position = "-1";
		time = -1;
	}
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	
	public long getTime() {
		return time;
	}
	
	public void setTime(long time) {
		this.time = time;
	}
	/**
	 * On compare les deux positions pour savoir la quelle est la plus recente
	 * on retourne la plus recente
	 * */
	public SiloPos newer(SiloPos s) {
		if(this.getTime()>s.getTime()) {
			return this;
		}
		return s;
	}
	@Override
	public String toString() {
		return "SiloPos [position=" + position + ", time=" + time + "]";
	}
	public boolean met() {
		// TODO Auto-generated method stub
		return time>0;
	}
	
	public void setAll(String pos,long time) {
		this.position = pos ;
		this.time = time;
	}
	
}
