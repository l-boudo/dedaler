package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;

public class Tresor implements Serializable{
	private int strength= 0;
	private int lockpick= 0;
	private int value= 0;
	private Observation type;
	private boolean open= false;
	private String pos;
	private boolean still_exist=true;
	private List<String> pickOrder;
	private int lastPick=-1;
	
	
	public Tresor(int lockpick, String pos) {
		this.lockpick = lockpick;
		this.pos = pos;
	}

	public Tresor(Couple<String,List<Couple<Observation,Integer>>> tresh) {
		pos = tresh.getLeft();
		for(Couple<Observation,Integer> couple : tresh.getRight()) {
			if(couple.getLeft().equals(Observation.STRENGH)) {
				strength = couple.getRight();
			}
			else if(couple.getLeft().equals(Observation.LOCKPICKING)) {
				lockpick = couple.getRight();

			}
			else if(couple.getLeft().equals(Observation.LOCKSTATUS)) {
				open = couple.getRight() == 1;

			}
			else if(couple.getLeft().equals(Observation.GOLD) || couple.getLeft().equals(Observation.DIAMOND)) {
				type = couple.getLeft();
				value = couple.getRight();
			}
		}
	}
	
	public void setPick(int order,String agent) {
		if(pickOrder == null) {
			pickOrder = new ArrayList<String>();
		}
		pickOrder.add(order-1, agent);
	}
	
	public int myOrder(String agent) {
		return pickOrder.indexOf(agent)+1;
	}
	
	public int getLastPick() {
		//0 = le cycle de prise est boucl� ou n'a pas commenc�
		return lastPick+1;
	}
	
	public void updateLastPick() {
		if(getLastPick() >= pickOrder.size()) {
			lastPick = -2;
		}else {
			lastPick +=1;
		}
	}
	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getLockpick() {
		return lockpick;
	}

	public void setLockpick(int lockpick) {
		this.lockpick = lockpick;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	public Observation getType() {
		return type;
	}

	public void setType(Observation type) {
		this.type = type;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}
	public boolean still_exist() {
		return still_exist;
	}

	public void setStill_exist(boolean still_exist) {
		this.still_exist = still_exist;
	}

	@Override
	public String toString() {
		return "Tresor [strength=" + strength + ", lockpick=" + lockpick + ", value=" + value + ", type=" + type
				+ ", open=" + open + ", pos=" + pos + ", still_exist=" + still_exist + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (open ? 1231 : 1237);
		result = prime * result + ((pos == null) ? 0 : pos.hashCode());
		result = prime * result + (still_exist ? 1231 : 1237);
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tresor other = (Tresor) obj;
		if (open != other.open)
			return false;
		if (pos == null) {
			if (other.pos != null)
				return false;
		} else if (!pos.equals(other.pos))
			return false;
		if (still_exist != other.still_exist)
			return false;
		if (value != other.value)
			return false;
		return true;
	}

}
