package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;

public class Tresor_test implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1661176637767470757L;
	private int strength;
	private int lockpick;
	private int value;
	private boolean open;
	private String pos;
	private boolean still_exist=true;
	private int lastPick=-1;
	public Tresor_test(int strength, int lockpick, int value, boolean open, String pos) {
		super();
		this.strength = strength;
		this.lockpick = lockpick;
		this.value = value;
		this.open = open;
		this.pos = pos;
	}
	@Override
	public String toString() {
		return "Tresor_test [strength=" + strength + ", lockpick=" + lockpick + ", value=" + value + ", open=" + open
				+ ", pos=" + pos + ", still_exist=" + still_exist + ", lastPick=" + lastPick + "]";
	}
	


}
