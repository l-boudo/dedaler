package eu.su.mas.dedaleEtu.mas.mybehaviours.collectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.ExploreSoloAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.behaviours.RandomWalkBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;


/**
 * This behaviour allows an agent to explore the environment and learn the associated topological map.
 * The algorithm is a pseudo - DFS computationally consuming because its not optimised at all.</br>
 * 
 * When all the nodes around him are visited, the agent randomly select an open node and go there to restart its dfs.</br> 
 * This (non optimal) behaviour is done until all nodes are explored. </br> 
 * 
 * Warning, this behaviour does not save the content of visited nodes, only the topology.</br> 
 * Warning, this behaviour is a solo exploration and does not take into account the presence of other agents (or well) and indefinitely tries to reach its target node
 * @author hc
 *
 */
public class CollectSoloBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private boolean finished = false;

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	/**
	 * Nodes known but not yet visited
	 */
	private List<String> openNodes;
	/**
	 * Visited nodes
	 */
	private Set<String> closedNodes;
	/**
	 *False counter 
	 */
	private int falser = 0;
	/**
	 *poids pour l'attente
	 */
	
	private int poids;
	/**
	 * Nodes with Treasure
	 */
	private Set<Tresor> tresors;
	private Set<Tresor> tresors_portables;
	private Tresor current_objective ;

	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresureFound;

    private Random rand = new Random();
    
    private int strength = 0;
    
	private Iterator<Tresor> it;

	private int exitValue= -1;
	public CollectSoloBehaviour(final AbstractDedaleAgent myagent, MapRepresentation myMap,Set<String> closedNodes, List<String> openNodes,int poids,  Set<Tresor> tresors) {
		
		super(myagent);
		this.myMap=myMap;
		this.poids = poids;
		this.tresors = tresors;


	}

	@Override
	public void action() {
		try {
			this.myAgent.doWait(150);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(it==null) {
			it = tresors.iterator();
		}
		triTresor();
		if(current_objective == null) {
			exitValue  = 2;
			finished = true;
		}
		else {
			//System.out.println("JE RAMASSE TACTIC !");
			//System.out.println(current_objective);

			//0) Retrieve the current position
			String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
			if(myPosition.equals(current_objective.getPos())) {
				if(true /* si je suis celui qui doit prendre le tresor au moment donn */) {
					boolean exists = false;
					int picked = ((AbstractDedaleAgent) this.myAgent).pick();
					System.out.println(this.myAgent.getLocalName()+" - The agent grabbed :"+((AbstractDedaleAgent) this.myAgent).pick());
					List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
					List<Couple<Observation,Integer>> lObservations= lobs.get(0).getRight();
					for(Couple<Observation,Integer> o:lObservations){
						switch (o.getLeft()) {
						case DIAMOND:case GOLD:
							exists = true;
							current_objective.setValue(o.getRight());
						}
					}
//					System.out.println(this.myAgent.getLocalName()+" Current "+current_objective);

					//current_objective.setValue(0);
//					System.out.println(this.myAgent.getLocalName()+" CurrentALL "+tresors);
					tresors.remove(current_objective);
					current_objective.setStill_exist(exists);
//					System.out.println(this.myAgent.getLocalName()+" Current "+current_objective);

					tresors.add(current_objective);
//					System.out.println(this.myAgent.getLocalName()+" CurrentALL2 "+tresors);

					// mettre a jour le tresor pris
					if(((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace() == 0) {
						exitValue = 1;
						finished = true;
						System.out.println(this.myAgent.getLocalName()+" :- Y a plus de place dans mon sac !!");
					}
					if(!current_objective.still_exist()) {
						
						//System.out.println(this.myAgent.getLocalName()+" "+current_objective);
						if(it.hasNext()) {
							current_objective = it.next();
						}
						else {
							// le trsore a ete de placer
							current_objective = null;
						}
					}
				}
			}
			else {
//				System.out.println(myPosition);
//				System.out.println(current_objective.getPos());
				try {
					String nextNode = this.myMap.getShortestPath(myPosition, current_objective.getPos()).get(0);
					//The move action (if any) should be the last action of your behaviour
					boolean pass= ((AbstractDedaleAgent)this.myAgent).moveTo(nextNode);	
					if(!pass) {
						falser ++;
					}
					if(falser>6) {
						exitValue = 2;
						falser = 0;
						finished = true;
					}
					
				} catch (Exception e) {
					System.out.println(((AbstractDedaleAgent)this.myAgent).getLocalName()+" :- J'ai pas LA CARTE POUR LE TRESOR !!");
					exitValue = 2;
					falser = 0;
					finished = true;
					}
			}
		}			
	}
				
			

		

	@Override
	public boolean done() {
		
		return finished;
	}
	
	public int onEnd() {
		/*
		 * On remet finished a false pour pouvoir r�excuter le behavior lorsque l'on reviendra a cet etat 
		 * */
		finished = false;
		return exitValue ;
		
	}
	private static int getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	private void triTresor() {
		List<String> pos_dejapris = new ArrayList<String>();
		for(Couple<Observation, Integer> exp : ((AbstractDedaleAgent)this.myAgent).getMyExpertise()) {
			if(exp.getLeft().equals(Observation.STRENGH)) {
				strength = exp.getRight();
			}
		}
		tresors_portables = new HashSet<Tresor>();
		for(Tresor t : tresors) {
			if(		t.isOpen() && t.still_exist()
					&& (strength>=t.getStrength())
					&& ((AbstractDedaleAgent) this.myAgent).getMyTreasureType().equals(t.getType())
					) {
				tresors_portables.add(t);
			}
			if(!t.still_exist()) {
				pos_dejapris.add(t.getPos());					
			}
		}
		this.it = tresors_portables.iterator();
		if(it.hasNext()) {
			current_objective = it.next();

		}else {
			
			current_objective = null;
		}
		if(!tresors.isEmpty()) {
			Iterator<Tresor> it2 = tresors.iterator();
			
			while (it2.hasNext()) {
				Tresor t = it2.next();
				if(pos_dejapris.contains(t.getPos())) {
					it2.remove();					
				}
				
			}
			
			//System.out.println("COLO "+tresors);
		}

//		System.out.println(this.myAgent.getLocalName()+" COLO "+tresors_portables);
		
	}
}
