package eu.su.mas.dedaleEtu.mas.mybehaviours.collectors;

import java.util.List;
import java.util.Random;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.SiloPos;
import eu.su.mas.dedaleEtu.mas.mybehaviours.general.RandomWalkPlus;
import jade.core.behaviours.SimpleBehaviour;

public class SearchSiloRandom extends SimpleBehaviour implements RandomWalkPlus{

	private boolean finished;
	private int exitValue=0;
	private SiloPos siloPos ;
	private String lastpos = null;

	private List<SiloPos> siloPos1;
	private boolean keep=false;
	private int falser=0;
	private boolean pass;
	public SearchSiloRandom(SiloPos siloPos) {
		// TODO Auto-generated constructor stub
		this.siloPos = siloPos;
	}

	public SearchSiloRandom(List<SiloPos> siloPos1) {
		// TODO Auto-generated constructor stub
		this.siloPos1 = siloPos1;

	}

	@Override
	public void action() {
		//System.out.println("J'AI UN TR�SOR !!!!!");
		if(exitValue != 0) {
			finished = false;
			exitValue = 0;
		}

		/**
		 * Just added here to let you see what the agent is doing, otherwise he will be too quick
		 */
		try {
			this.myAgent.doWait(150);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * TODO : Faire Le calcule des differentes coalitions possibles 
		 * */
		
		//Example to retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			
			((AbstractDedaleAgent)this.myAgent).emptyMyBackPack("Silo");
			
			if(((AbstractDedaleAgent)this.myAgent).getBackPackFreeSpace()>0 && !siloPos1.get(0).met()) {
				siloPos1.get(0).setAll(myPosition, System.currentTimeMillis()/1000);
				siloPos1.set(0,siloPos1.get(0));
				System.out.println(((AbstractDedaleAgent)this.myAgent).getLocalName()+" :- J'ai de la Place !!");
				exitValue = 1;
				finished = true;
			}
			else if(siloPos1.get(0).met()){
				System.out.println(((AbstractDedaleAgent)this.myAgent).getLocalName()+" :- JE DEVIENS TACTIC ");
				finished = true;
				exitValue = 2;
			}
			//Random move from the current position
			
			if(!keep) {
				randomLobs(lastpos,lobs);
			}
			else {
				falser  ++;
				if(falser>20) {
					keep = false;
					falser = 0;
				}
			}			lastpos = myPosition;

			Random r= new Random();
			int moveId=1+r.nextInt(lobs.size()-1);//removing the current position from the list of target, not necessary as to stay is an action but allow quicker random move

			//The move action (if any) should be the last action of your behaviour
			pass = ((AbstractDedaleAgent)this.myAgent).moveTo(lobs.get(moveId).getLeft());
			if(!pass && !keep) {
				falser++;
				if(falser>5) {
					System.out.println(this.myAgent.getLocalName()+" :- Chui Bloqué !!");
					keep = true;
					falser = 0;
					}
				}			
		}
		
	}
	
	public int onEnd() {
		return exitValue ;
		
	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}
