package eu.su.mas.dedaleEtu.mas.mybehaviours.collectors;

import java.util.List;
import java.util.Random;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.SiloPos;
import jade.core.behaviours.SimpleBehaviour;

public class SearchSiloTactic extends SimpleBehaviour{

	private boolean finished;
	private int exitValue=0;
	SiloPos siloPos;
	private MapRepresentation myMap;
	private int falser = 0;
	private List<SiloPos> siloPos1;
	public SearchSiloTactic(SiloPos siloPos,MapRepresentation myMap) {
		// TODO Auto-generated constructor stub
		this.myMap = myMap;
		this.siloPos = siloPos;
	}
	public SearchSiloTactic(List<SiloPos> siloPos1,MapRepresentation myMap) {
		// TODO Auto-generated constructor stub
		this.myMap = myMap;
		this.siloPos1 = siloPos1;
	}
	@Override
	public void action() {
		//System.out.println("J'AI UN TR�SOR !!!!!");
		/**
		 * Just added here to let you see what the agent is doing, otherwise he will be too quick
		 */
		try {
			this.myAgent.doWait(150);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * TODO : Faire Le calcule des differentes coalitions possibles 
		 * */
		
		//Example to retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if (myPosition!=null){
			siloPos = siloPos1.get(0);
			List<String> lesNodes = null;
			try {
				
				lesNodes = this.myMap.getShortestPath(myPosition, siloPos.getPosition());	
				
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(((AbstractDedaleAgent)this.myAgent).getLocalName()+" :- J'ai pas LA CARTE pour trouver le Silo !");
				exitValue = 2;
				falser = 0;
				finished = true;
				return;

			}

			
			//List of observable from the agent's current position
			
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			((AbstractDedaleAgent)this.myAgent).emptyMyBackPack("Silo");	
			if(((AbstractDedaleAgent)this.myAgent).getBackPackFreeSpace()>0) {
				siloPos.setAll(myPosition, System.currentTimeMillis()/1000);
				siloPos1.set(0, siloPos);
				System.out.println("J'ai de la Place !!");
				exitValue = 1;
				finished = true;
			}
			if(lesNodes.isEmpty()) {
				// on repasse au random
				siloPos.setAll("-1", -1);
				siloPos1.set(0, siloPos);
				exitValue = 3;
				finished = true;
			}
			else {				
				//The move action (if any) should be the last action of your behaviour
				boolean pass=((AbstractDedaleAgent)this.myAgent).moveTo(lesNodes.get(0));
				if(!pass) {
					falser ++;
				}
				if(falser>6) {
					exitValue = 2;
					falser = 0;
					finished = true;
				}
			}
			
		}
		
	}
	
	public int onEnd() {
		int realExitValue = exitValue;
		finished = false;
		exitValue = 0;
		return realExitValue ;
		
	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}
}
