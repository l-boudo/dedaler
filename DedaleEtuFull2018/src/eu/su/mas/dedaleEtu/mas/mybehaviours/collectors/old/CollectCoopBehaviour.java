package eu.su.mas.dedaleEtu.mas.mybehaviours.collectors.old;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.ExploreSoloAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.behaviours.RandomWalkBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;


/**
 * This behaviour allows an agent to explore the environment and learn the associated topological map.
 * The algorithm is a pseudo - DFS computationally consuming because its not optimised at all.</br>
 * 
 * When all the nodes around him are visited, the agent randomly select an open node and go there to restart its dfs.</br> 
 * This (non optimal) behaviour is done until all nodes are explored. </br> 
 * 
 * Warning, this behaviour does not save the content of visited nodes, only the topology.</br> 
 * Warning, this behaviour is a solo exploration and does not take into account the presence of other agents (or well) and indefinitely tries to reach its target node
 * @author hc
 *
 */
public class CollectCoopBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private boolean finished = false;

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	/**
	 * Nodes known but not yet visited
	 */
	private List<String> openNodes;
	/**
	 * Visited nodes
	 */
	private Set<String> closedNodes;
	/**
	 *False counter 
	 */
	private int falser = 0;
	/**
	 *poids pour l'attente
	 */
	
	private int poids;
	/**
	 * Nodes with Treasure
	 */
	private Set<Tresor> tresors;
	
	private Tresor current_objective ;

	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresureFound;

    private Random rand = new Random();

	private int exit_value = -1;
	public CollectCoopBehaviour(final AbstractDedaleAgent myagent, MapRepresentation myMap,Set<String> closedNodes, List<String> openNodes,int poids, Set<Couple<String, List<Couple<Observation, Integer>>>> treasureFound) {
		super(myagent);
		this.myMap=myMap;
		this.poids = poids;
		this.tresors = tresors;
		current_objective = tresors.iterator().next();

	}

	@Override
	public void action() {
		
		if(current_objective == null) {
			exit_value  = 2;
			finished = true;
		}
		else {

			//0) Retrieve the current position
			String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
			if(false /* si tresor est bien a ma pos */ ) {
				if(false /* si je suis celui qui doit prendre le tresor au moment donn� */) {
					System.out.println(this.myAgent.getLocalName()+" - The agent grabbed :"+((AbstractDedaleAgent) this.myAgent).pick());
					// mettre a jour le tresor pris
				}
				else { // on reste pour que l'autre recupere le tresor
					
				}
			}
			String nextNode = this.myMap.getShortestPath(myPosition, current_objective.getPos()).get(0);
			boolean pass= ((AbstractDedaleAgent)this.myAgent).moveTo(nextNode);
			
			// interblockage
			
			//
			
		}
				
			

		
	}

	@Override
	public boolean done() {
		
		return finished;
	}
	private static int getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

}
