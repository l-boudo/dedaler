package eu.su.mas.dedaleEtu.mas.mybehaviours.collectors.old;

import java.util.List;
import java.util.Random;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.behaviours.SimpleBehaviour;

public class CollectRandom extends SimpleBehaviour{
	/**
	 * When an agent choose to move
	 *  
	 */
	private int exitValue = 0;
	private boolean finished;
	private String lastpos;
	private static final long serialVersionUID = 9088209402507795289L;

	public CollectRandom (final AbstractDedaleAgent myagent) {
		super(myagent);
		//super(myagent);
		lastpos = null;
	}

	@Override
	public void action() {
		exitValue = 0;

		//Example to retrieve the current position
		/**
		 * Just added here to let you see what the agent is doing, otherwise he will be too quick
		 */
		try {
			this.myAgent.doWait(200);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

		if (myPosition!=""){
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			//System.out.println(this.myAgent.getLocalName()+" -- list of observables: "+lobs);

			//list of observations associated to the currentPosition
			List<Couple<Observation,Integer>> lObservations= lobs.get(0).getRight();

			//example related to the use of the backpack for the treasure hunt
			//System.out.println(exitValue);
			for(Couple<Observation,Integer> o:lObservations){
				switch (o.getLeft()) {
				case DIAMOND:case GOLD:
					/*
					 * Un test qui met le exitValue a 1 et finished a true => pour pouvoir chang� d'�tat
					 * 
					 * */
					System.out.println(this.myAgent.getLocalName()+" :- Y a un tresor !!");

//					System.out.println(this.myAgent.getLocalName()+" - My treasure type is : "+((AbstractDedaleAgent) this.myAgent).getMyTreasureType());
//					System.out.println(this.myAgent.getLocalName()+" - My current backpack capacity is:"+ ((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace());
//					System.out.println(this.myAgent.getLocalName()+" - Value of the treasure on the current position: "+o.getLeft() +": "+ o.getRight());
//					System.out.println(this.myAgent.getLocalName()+" - The agent grabbed :"+((AbstractDedaleAgent) this.myAgent).pick());
//					System.out.println(this.myAgent.getLocalName()+" - the remaining backpack capacity is: "+ ((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace());
					((AbstractDedaleAgent) this.myAgent).pick();
					if(((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace() == 0) {
						exitValue = 1;
						finished = true;
						System.out.println(this.myAgent.getLocalName()+" :- Y a plus de place dans mon sac !!");
					}
					break;
				default:
					break;
				}
			}
			//Trying to store everything in the tanker
			System.out.println("Je suis a: "+myPosition);
			System.out.println("Je voiyait : "+lobs);
			if(exitValue != 1) {
				exitValue = 2;
				finished = true;
			}
			//Random move from the current position
			Random r= new Random();
			/*
			 * Meilleur random pour vraimement aller partout
			 * */
			if(lastpos!=null && lobs.size()>2) {
				System.out.println("Last : "+lastpos);
				for(Couple<String, List<Couple<Observation, Integer>>> o : lobs) {
					if(o.getLeft().equals(lastpos)) {
						lobs.remove(o);
						break;
					}
				}
			}
			System.out.println("Je vois : "+lobs);
			int moveId=1+r.nextInt(lobs.size()-1);//removing the current position from the list of target to accelerate the tests, but not necessary as to stay is an action

			//The move action (if any) should be the last action of your behaviour
			((AbstractDedaleAgent)this.myAgent).moveTo(lobs.get(moveId).getLeft());
			lastpos = myPosition;

		}

	}

	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		
		return finished;
	}
	public int onEnd() {
		/*
		 * On remet finished a false pour pouvoir r�excuter le behavior lorsque l'on reviendra a cet etat 
		 * */
		finished = false;
		return exitValue ;
		
	}
}