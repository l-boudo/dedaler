package eu.su.mas.dedaleEtu.mas.mybehaviours.explorators;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.Coalition;
import eu.su.mas.dedaleEtu.mas.knowledge.Member;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;

/**************************************
 * 
 * 
 * 				BEHAVIOUR RandomWalk : Illustrates how an agent can interact with, and move in, the environment
 * 
 * 
 **************************************/


public class CalculeCoop extends SimpleBehaviour{
	
	/**
	 * When an agent choose to move
	 *  
	 */
	private static final long serialVersionUID = 9088209402507795289L;
	private List<Member> allExpert;
	private List<Coalition> allCoals = null;
	private Coalition maxCoal = null;

	private Set<Tresor> tresors;
	private boolean finished = false;
	private int exitValue = 0;

	public CalculeCoop (final AbstractDedaleAgent myagent,  List<Member> allExpert) {
		super(myagent);
		this.allExpert= allExpert;
	}

	public void action() {
			if(allCoals == null) {
			int nombre = ((AbstractDedaleAgent)this.myAgent).getLocalName().charAt(5);
			allCoals = Coalition.generate_coaltions_wu(allExpert, 1,tresors,true);
			//maxCoal = Coalition.getMax(allCoals);
		}


	}
	public int onExit() {
		return exitValue;
	}
	@Override
	public boolean done() {
		// TODO Auto-generated method stub
		return finished;
	}

}