package eu.su.mas.dedaleEtu.mas.mybehaviours.explorators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.ExploreSoloAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.behaviours.RandomWalkBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;


/**
 * This behaviour allows an agent to explore the environment and learn the associated topological map.
 * The algorithm is a pseudo - DFS computationally consuming because its not optimised at all.</br>
 * 
 * When all the nodes around him are visited, the agent randomly select an open node and go there to restart its dfs.</br> 
 * This (non optimal) behaviour is done until all nodes are explored. </br> 
 * 
 * Warning, this behaviour does not save the content of visited nodes, only the topology.</br> 
 * Warning, this behaviour is a solo exploration and does not take into account the presence of other agents (or well) and indefinitely tries to reach its target node
 * @author hc
 *
 */
public class ExploCoopBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private boolean finished = false;

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	/**
	 * Nodes known but not yet visited
	 */
	private List<String> openNodes;
	/**
	 * Visited nodes
	 */
	private Set<String> closedNodes;
	/**
	 *False counter 
	 */
	private int falser = 0;
	/**
	 *poids pour l'attente
	 */
	
	private int poids;
	/**
	 *exitValue pour le fsm
	 */
	
	private int exitValue = 0;
	/**
	 * Nodes with Treasure
	 */
	private Set<Tresor> tresors;

	
	
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorClose;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorOpen;

    private Random rand = new Random();
	public ExploCoopBehaviour(
			final AbstractDedaleAgent myagent,
			MapRepresentation myMap,
			Set<String> closedNodes,
			List<String> openNodes,
			Set<Tresor> tresors,
			int poids,
			Set<Couple<String, List<Couple<Observation, Integer>>>> tresorClose,
			Set<Couple<String, List<Couple<Observation, Integer>>>> tresorOpen
			) {
		super(myagent);
		this.myMap=myMap;
		this.closedNodes=closedNodes;
		this.openNodes = openNodes;
		this.tresors = tresors;
//		closedNodes=new HashSet<String>();
//		openNodes=new ArrayList<String>();
		this.poids = poids;
		this.tresorClose = tresorClose;
		this.tresorOpen = tresorOpen;

	}
	public List tresorPos(Set<Couple<String,List<Couple<Observation,Integer>>>> tresor){
		List<String> pos = new ArrayList<String>();
		for(Couple<String,List<Couple<Observation,Integer>>> c : tresor) {
			pos.add(c.getLeft());
		}
		return pos;
		
	}
	@Override
	public void action() {
		if(exitValue != 0) {
			finished = false;
			exitValue = 0;
		}
		if(this.myMap==null)
			this.myMap= new MapRepresentation();

		/**
		 * Just added here to let you see what the agent is doing, otherwise he will be too quick
		 */
		try {
			this.myAgent.doWait(200);
		} catch (Exception e) {
			e.printStackTrace();
		}

		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
	
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			for(int i = 0; i<lobs.size();i++) {
				if(!(lobs.get(i).getRight().isEmpty()) ) {
					if(!(lobs.get(i).getRight().get(0).getLeft().equals(Observation.STENCH))){
//					Ouverture de coffre et ajout dans la liste de tresors(type Tresor):
					boolean openlock = ((AbstractDedaleAgent)this.myAgent).openLock(lobs.get(i).getRight().get(0).getLeft());
//					System.out.println(openlock); 
					lobs=((AbstractDedaleAgent)this.myAgent).observe();
					
					((AbstractDedaleAgent)this.myAgent).openLock(lobs.get(i).getRight().get(0).getLeft()); 
					lobs=((AbstractDedaleAgent)this.myAgent).observe();
//					System.out.println(lobs);
					Tresor tresue = new Tresor(lobs.get(i));
					if(!tresors.contains(tresue)) {
						tresors.add(tresue);						
					}
					System.out.println(this.myAgent.getLocalName()+" :- Je suis passé sur "+tresue);
					}else {
						System.out.println(this.myAgent.getLocalName()+" :- Y a le GOLEM PAS LOIN!!");

					}
				}
			}
			//System.out.println("Moi "+this.myAgent.getLocalName()+" J'observe que "+lobs.toString());

			//1) remove the current node from openlist and add it to closedNodes.
			this.closedNodes.add(myPosition);
			this.openNodes.remove(myPosition);

			this.myMap.addNode(myPosition);

			//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
			String nextNode=null;
			Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
			while(iter.hasNext()){
				String nodeId=iter.next().getLeft();
				if (!this.closedNodes.contains(nodeId)){
					if (!this.openNodes.contains(nodeId)){
						this.openNodes.add(nodeId);
						this.myMap.addNode(nodeId, MapAttribute.open);
						this.myMap.addEdge(myPosition, nodeId);	
					}else{
						//the node exist, but not necessarily the edge
						this.myMap.addEdge(myPosition, nodeId);
					}
					if (nextNode==null) nextNode=nodeId;
				}
			}

			//3) while openNodes is not empty, continues.
			if (this.openNodes.isEmpty()){
				//Explo finished
				exitValue = 3;
				finished=true;
				System.out.println(this.myAgent.getLocalName() +" Exploration successufully done, behaviour removed.");
				System.out.println("My "+this.myAgent.getLocalName() +" I found "+tresors+" and i open "+tresorOpen);
		}else{
				//4) select next move.
				//4.1 If there exist one open node directly reachable, go for it,
				//	 otherwise choose one from the openNode list, compute the shortestPath and go for it
				try {
					if (nextNode==null){
						//no directly accessible openNode
						//chose one, compute the path and take the first step.	
						nextNode=this.myMap.getShortestPath(myPosition, this.openNodes.get(0)).get(0);
					}
					
					
					//en cas de blocage entre deux agents
					/*
					 * On réessaye n fois de réacceder a la case que l'on voulais a la base
					 * 
					 * */
					boolean pass= ((AbstractDedaleAgent)this.myAgent).moveTo(nextNode);
					if(!pass) {
						falser++;
					}
					
					if(falser>poids) {
						exitValue = 2;
						falser = 0;
						finished = true;
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					System.out.println(this.myAgent.getLocalName()+" "+myPosition);
					System.out.println(this.myAgent.getLocalName()+" ouvert "+this.openNodes);
					System.out.println(this.myAgent.getLocalName()+" ferme "+this.closedNodes);
					System.out.println(this.myAgent.getLocalName()+" "+this.openNodes.get(0));
					exitValue = 3;
					finished=true;
					System.out.println(this.myAgent.getLocalName() +" Exploration successufully done, behaviour removed.");
					System.out.println("My "+this.myAgent.getLocalName() +" I found "+tresors+" and i open "+tresorOpen);
				}
				
			}

		}
	}

	@Override
	public boolean done() {
		// l'exploration est finie
		// exitValue = 1;
		return finished;
	}
	public int onEnd() {
		return exitValue;
	}
	private static int getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

}
