package eu.su.mas.dedaleEtu.mas.mybehaviours.explorators;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;

/**************************************
 * 
 * 
 * 				BEHAVIOUR RandomWalk : Illustrates how an agent can interact with, and move in, the environment
 * 
 * 
 **************************************/


public class RandomExploAntiBlocage extends SimpleBehaviour{
	
	/**
	 * When an agent choose to move
	 *  
	 */
	private static final long serialVersionUID = 9088209402507795289L;
	private Set<Couple<String, Set<Couple<Observation, Integer>>>> allExpertises;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorClose;

	private boolean finished = false;
	private int falser = 0;
	private int exitValue = 0;

	public RandomExploAntiBlocage (final AbstractDedaleAgent myagent, Set<Couple<String, Set<Couple<Observation, Integer>>>> allExpertises) {
		super(myagent);
		this.allExpertises= allExpertises;
	}

	public void action() {
		exitValue = 0;
		finished = false;
		/**
		 * Just added here to let you see what the agent is doing, otherwise he will be too quick
		 */
		try {
			this.myAgent.doWait(500);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * TODO : Faire Le calcule des differentes coalitions possibles 
		 * */
		
		//Example to retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if (myPosition!=null){
			falser ++;
			if(falser > 20 ) {
				exitValue = 1;
				finished = true;
			}
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			
			//TODO : Ajouter une contrainte sur les noeuds choisi (par ex on exclu les 2 derniers dans le cas ou la)
			
			//Random move from the current position
			Random r= new Random();
			int moveId=1+r.nextInt(lobs.size()-1);//removing the current position from the list of target, not necessary as to stay is an action but allow quicker random move

			//The move action (if any) should be the last action of your behaviour
			((AbstractDedaleAgent)this.myAgent).moveTo(lobs.get(moveId).getLeft());

			}

	}
	public int onEnd() {
		falser = 0;
		finished = false;
		return exitValue;
	}
	@Override
	public boolean done() {
		return finished;
	}

}