package eu.su.mas.dedaleEtu.mas.mybehaviours.explorators;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;
import eu.su.mas.dedaleEtu.mas.mybehaviours.general.RandomWalkPlus;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;

/**************************************
 * 
 * 
 * 				BEHAVIOUR RandomWalk : Illustrates how an agent can interact with, and move in, the environment
 * 
 * 
 **************************************/


public class RandomExploCoop extends SimpleBehaviour  implements RandomWalkPlus{
	
	/**
	 * When an agent choose to move
	 *  
	 */
	private static final long serialVersionUID = 9088209402507795289L;
	private Set<Couple<String, Set<Couple<Observation, Integer>>>> allExpertises;
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresorClose;

	private boolean finished = false;
	private int exitValue = 0;
	private String  lastpos;
	private boolean pass;
	private boolean keep = false;
	private Set<Tresor> tresors;

	private int falser = 0;

	public RandomExploCoop (final AbstractDedaleAgent myagent, Set<Couple<String, Set<Couple<Observation, Integer>>>> allExpertises,Set<Tresor> tresors) {
		super(myagent);
		this.allExpertises= allExpertises;
		this.tresors = tresors;
	}

	public void action() {
		
		if(exitValue != 0) {
			finished = false;
			exitValue = 0;
		}
		/**
		 * Just added here to let you see what the agent is doing, otherwise he will be too quick
		 */
		try {
			this.myAgent.doWait(200);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * TODO : Verifier que l'on est toutes les expertises pour pouvoir faire le calcule des differentes coalitions possibles 
		 * */
		
		if(allExpertises.size() == 3) {
			finished = true;
			exitValue = 1;
		}
		
		//Example to retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			for(int i = 0; i<lobs.size();i++) {
				if(!(lobs.get(i).getRight().isEmpty())) {
					if(!(lobs.get(i).getRight().get(0).getLeft().equals(Observation.STENCH))){

//					Ouverture de coffre et ajout dans la liste de tresors(type Tresor):
					boolean openlock = ((AbstractDedaleAgent)this.myAgent).openLock(lobs.get(i).getRight().get(0).getLeft());
//					System.out.println(openlock); 
					lobs=((AbstractDedaleAgent)this.myAgent).observe();
					try {
						((AbstractDedaleAgent)this.myAgent).openLock(lobs.get(i).getRight().get(0).getLeft()); 
						
						lobs=((AbstractDedaleAgent)this.myAgent).observe();
//					System.out.println(lobs);
						Tresor tresue = new Tresor(lobs.get(i));
						
						if(!tresors.contains(tresue)) {
							tresors.add(tresue);						
						}
						System.out.println(this.myAgent.getLocalName()+" :- Je suis passé sur "+tresue);
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(this.myAgent.getLocalName()+" :- J'ai eu un pb dans mon EXPLO !");

					}
				}else {
					System.out.println(this.myAgent.getLocalName()+" :- Y a le GOLEM PAS LOIN!!");

				}
					}
				
			}
			//Random move from the current position
			Random r= new Random();
			if(!keep) {
				randomLobs(lastpos,lobs);
			}
			else {
				falser++;
				if(falser>20) {
					keep = false;
					falser = 0;
				}
			}
			
			lastpos = myPosition;
			int moveId=1+r.nextInt(lobs.size()-1);//removing the current position from the list of target, not necessary as to stay is an action but allow quicker random move

			//The move action (if any) should be the last action of your behaviour
			pass = ((AbstractDedaleAgent)this.myAgent).moveTo(lobs.get(moveId).getLeft());
			if(!pass && !keep) {
				falser++;
				if(falser>5) {
					System.out.println(this.myAgent.getLocalName()+" :- Chui Bloqué !!");
					keep = true;
					falser = 0;
					}
			}
//			else {
//				keep = false;
//				falser = 0;
//			}
		}

	}
	public int onExit() {
		return exitValue;
	}
	@Override
	public boolean done() {
		return finished;
	}

}