package eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.old;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.ExploreSoloAgent;
import eu.su.mas.dedaleEtu.mas.agents.yours.AgentExploreEnCoop;
import eu.su.mas.dedaleEtu.mas.behaviours.RandomWalkBehaviour;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;


/**
 * This behaviour allows an agent to explore the environment and learn the associated topological map.
 * The algorithm is a pseudo - DFS computationally consuming because its not optimised at all.</br>
 * 
 * When all the nodes around him are visited, the agent randomly select an open node and go there to restart its dfs.</br> 
 * This (non optimal) behaviour is done until all nodes are explored. </br> 
 * 
 * Warning, this behaviour does not save the content of visited nodes, only the topology.</br> 
 * Warning, this behaviour is a solo exploration and does not take into account the presence of other agents (or well) and indefinitely tries to reach its target node
 * @author hc
 *
 */
public class LockPickEnCoopBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private boolean finished = false;

	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	/**
	 * Nodes known but not yet visited
	 */
	private List<String> openNodes;
	/**
	 * Visited nodes
	 */
	private Set<String> closedNodes;
	/**
	 *False counter 
	 */
	private int falser = 0;
	/**
	 *poids pour l'attente
	 */
	private int poids;
	/**
	 * nodes avec des tresors
	 */
	private Set<Couple<String,List<Couple<Observation,Integer>>>> tresureToOpen;
	/**
	 * les objectifs a atteindre
	 * les objectifs atteints
	 */
	private int objective_current = 0 ; 
	private String objective_pos ; 
	private Observation objective_obs ; 
	private int objective_done = 0; 
	/**
	 * l'equipe dans la quelle se trouve l'agent
	 */
	private Set<String> myTeam;
	
	private Iterator<Couple<String, List<Couple<Observation, Integer>>>> tresureIte;
	private Couple<String, List<Couple<Observation, Integer>>> currentTresure;
    private Random rand = new Random();
	public LockPickEnCoopBehaviour(final AbstractDedaleAgent myagent, MapRepresentation myMap,Set<String> closedNodes, List<String> openNodes,int poids, Set<Couple<String, List<Couple<Observation, Integer>>>> treasureFound) {
		super(myagent);
		this.myMap=myMap;
		this.closedNodes=closedNodes;
		this.openNodes = openNodes;
		this.poids = poids;
		this.tresureToOpen = treasureFound;
		//objectives = tresureToOpen.size();
		Iterator<Couple<String, List<Couple<Observation, Integer>>>> tresureIte = tresureToOpen.iterator();
		
	}

	@Override
	public void action() {

		if(this.myMap==null)
			this.myMap= new MapRepresentation();

		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

		if (myPosition!=null) {
			
			
			/**
			 * On initialise l'objectif courant:
			 * - la pos 
			 * - l'observation
			 * */
			if( (objective_done == 0 && objective_current == 0) || (objective_current == 0 && objective_done == 1 ) ) {
				currentTresure = tresureIte.next();
				objective_pos = currentTresure.getLeft();
				objective_obs = currentTresure.getRight().get(0).getLeft();
				objective_current = 1 ;
			}
			
			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(500);
			} catch (Exception e) {
				e.printStackTrace();
			}

			//1) remove the current node from openlist and add it to closedNodes.

			//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
			String nextNode=null;

			// on choisi le noeud pour aller a la position du tresor
			nextNode=this.myMap.getShortestPath(myPosition, objective_pos).get(0);
			
			// Si nextNode vaut null cela veut dire que nous somme sur le tresor
			if(nextNode==null) {
				((AbstractDedaleAgent)this.myAgent).openLock(objective_obs);
			}
						
			//en cas de blocage entre deux agents
			/*
			 * On essaye n fois de re-aller a la case que l'on voulais a la base
			 * 
			 * */
			boolean pass= ((AbstractDedaleAgent)this.myAgent).moveTo(nextNode);
			
			if(!pass) {
				falser++;
			}
			
			if(falser>poids) {
				String arrive = this.openNodes.get(getRandomNumberInRange(0,openNodes.size()-1));
				List<String> lesNodes = this.myMap.getShortestPath(myPosition, arrive);
				nextNode=lesNodes.get(0);
				((AbstractDedaleAgent)this.myAgent).moveTo(nextNode);
				falser = 0;
			}
			
		}

	}
	

	@Override
	public boolean done() {
		
		return finished;
	}
	private static int getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

}
