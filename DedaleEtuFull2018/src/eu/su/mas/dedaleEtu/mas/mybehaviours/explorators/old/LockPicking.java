package eu.su.mas.dedaleEtu.mas.mybehaviours.explorators.old;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;

public class LockPicking extends OneShotBehaviour{

	private static AbstractDedaleAgent myagent;
	private boolean exitValue;
	public LockPicking(final AbstractDedaleAgent myagent) {
		super(myagent);

	}
	public void action() {
		// TODO Auto-generated method stub
		exitValue = ((AbstractDedaleAgent) myagent).openLock(null);
		System.out.println(myagent.getLocalName()+(exitValue ? " -: J'ai ouvert le coffre" : " -: J'ai pu ouvrir le coffre ")+null+" !");
				
	}
	public int onEnd() {
		return exitValue ? 1 : 0;
	}

}
