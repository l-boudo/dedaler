package eu.su.mas.dedaleEtu.mas.mybehaviours.general;

import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
public interface RandomWalkPlus {
	/**
	 * @param lastpos : La precedente position 
	 * @param lobs : La liste d'observation
	 * */
	public default List<Couple<String,List<Couple<Observation,Integer>>>> randomLobs(String lastpos,List<Couple<String,List<Couple<Observation,Integer>>>> lobs){
		if(lastpos!=null && lobs.size()>2) {
			//System.out.println("Last : "+lastpos);
			for(Couple<String, List<Couple<Observation, Integer>>> o : lobs) {
				if(o.getLeft().equals(lastpos)) {
					lobs.remove(o);
					break;
				}
			}
		}
		return lobs;
	}
}
