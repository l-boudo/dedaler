package eu.su.mas.dedaleEtu.princ.old;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedaleEtu.mas.knowledge.Coalition;
import eu.su.mas.dedaleEtu.mas.knowledge.Member;
import eu.su.mas.dedaleEtu.mas.knowledge.Tresor;

public class CoalitionTest {

	public static void main(String[] args) {
		List<Member> allExpert = new ArrayList<Member>();
		Set<Tresor> tresors = new HashSet<Tresor>();
		Tresor t1  =new Tresor(0,"1");
		Tresor t2  =new Tresor(5,"2");
		Tresor t3  =new Tresor(9,"3");

		tresors.add(t1);
		tresors.add(t2);
		tresors.add(t3);

		allExpert.add(new Member("Virgil",2));
		allExpert.add(new Member("Vandamme",2));
		allExpert.add(new Member("Violette",5));
		// TODO Auto-generated method stub
//		 ARGH !!!!!
//		Coalition.generate_struct20(new HashSet<Set<Member>>(),Coalition.generate_coaltions20(allExpert )) ;
		List<Coalition> coals1 = Coalition.generate_coaltions_wu(allExpert, 1,tresors,true);
		List<Coalition> coals2 = Coalition.generate_coaltions_wu(allExpert, 2,tresors,true);
		List<Coalition> coals3 = Coalition.generate_coaltions_wu(allExpert, 3,tresors,true);

		System.out.println(" * ");
		System.out.println(Coalition.getMax(coals1));
		System.out.println(Coalition.getMax(coals2));
		System.out.println(Coalition.getMax(coals3));
		System.out.println(" * ");


	}
}
